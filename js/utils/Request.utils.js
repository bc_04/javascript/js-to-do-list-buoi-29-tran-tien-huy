const BASE_URL = "https://62db6ca0d1d97b9e0c4f3326.mockapi.io/";

const AXIOS_INSTANCE = axios.create({
  baseURL: `${BASE_URL}`,
});

const API_ENDPOINT = "mainProducts";

const getAllProducts = () => {
  return AXIOS_INSTANCE.get(`/${API_ENDPOINT}`);
};

const getProductById = (id) => {
  return AXIOS_INSTANCE.get(`/${API_ENDPOINT}/${id}`);
};

export { getAllProducts, getProductById };
