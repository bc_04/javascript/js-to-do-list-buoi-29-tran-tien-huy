class RenderUtils {
  #callBackList;
  constructor() {
    this.target = "";
    this.targetData = "";
    this.#callBackList = {};
  }

  setTargetEl(targetEl) {
    this.target = document.querySelector(targetEl);
  }

  getTargetEl() {
    return this.target;
  }

  setTargetData(data) {
    this.targetData = data;
  }

  getTargetData() {
    return this.targetData;
  }

  setCallBackList(callBackList) {
    this.callBackList = { ...callBackList };
  }

  getCallBackList() {
    return this.callBackList;
  }

  /**
   * Create any HTML element by using rest parameter with a document element.
   * Returns the a html dom element.
   * @param el The target object to copy to.
   * @param {...props} Convert any type of props into an object to passs to the target document
   */
  createHTMLElement(el, { ...props }) {
    let htmlEl = document.createElement(el);
    Object.assign(htmlEl, props);
    return htmlEl;
  }

  createTodoListHTML() {
    let targetData = this.getTargetData();
    let targetEl = "";
    let { completeTodo, deleteTodo } = this.getCallBackList();
    document.querySelector("#todo").innerHTML = "";
    document.querySelector("#completed").innerHTML = "";
    targetData.forEach((todo, idx) => {
      targetEl =
        todo.status === 0
          ? document.querySelector("#todo")
          : document.querySelector("#completed");
      targetEl.innerHTML += `
        <li class="todo d-flex flex--center" id="todo-${idx}">
          <span>${todo.name}</span>
          <div class="buttons d-flex flex--center gap-10px">
            <div class="remove" data-idx=${idx}>
              <i class="fas fa-trash-alt" data-idx=${idx}></i>
            </div>
            <div class="complete" data-idx=${idx}>
              <i class="far fa-check-circle" data-idx=${idx}></i>
              <i class="fas fa-check-circle" data-idx=${idx}></i>
            </div>
          </div>
        </li>
      `;
    });

    document.querySelectorAll(".remove").forEach((btn) => {
      this.bindEvents(btn, "click", deleteTodo);
    });

    document.querySelectorAll(".complete").forEach((btn) => {
      this.bindEvents(btn, "click", completeTodo);
    });
  }

  bindEvents(_target, event, callback) {
    if (_target instanceof HTMLElement) {
      _target.addEventListener(event, callback);
    } else {
      document.querySelector(_target).addEventListener(event, callback);
    }
  }
}

export { RenderUtils };
