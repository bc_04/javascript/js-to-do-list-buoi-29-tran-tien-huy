class Validator {
  #_value = "";
  constructor() {}

  setValue(value) {
    this._value = value;
  }
  getValue() {
    return this._value;
  }

  required() {
    return this.getValue().length > 0;
  }
  minLength(length) {
    return this.getValue().length >= length;
  }
  maxLength(length) {
    return this.getValue().length <= length;
  }
}

export { Validator };
