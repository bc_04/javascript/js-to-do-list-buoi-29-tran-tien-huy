class Todo {
  constructor(name, status) {
    this.name = name;
    this.status = status;
  }
}

export { Todo };
