import { RenderUtils } from "../utils/Render.utils.js";
let renderObj = new RenderUtils();
let renderTodoList = (todoList, callBackList) => {
  renderObj.setCallBackList(callBackList);
  renderObj.setTargetData(todoList);
  renderObj.createTodoListHTML();
};

let sortAscedingOrder = (todoList) => {
  todoList.sort((a, b) => {
    if (a.name < b.name) {
      return -1;
    }

    if (a.name > b.name) {
      return 1;
    }

    return 0;
  });
};
let sortDescedingOrder = (todoList) => {
  todoList.sort((a, b) => {
    if (a.name > b.name) {
      return -1;
    }

    if (a.name < b.name) {
      return 1;
    }

    return 0;
  });
};

export { renderTodoList, sortAscedingOrder, sortDescedingOrder };
