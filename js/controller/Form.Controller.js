class FormController {
  #_formFieldInstances = [];
  #__rules = [];
  #_errors = {};
  #_DEFAULT_ERROR_MESSAGE = {
    required: "Please do not leave this field empty",
    validEmail: "Pleasae enter a correct format email",
    minLength: "Please enter at least {para} character",
    maxLength: "Please enter maximum {para} character",
  };
  constructor(formSelector) {
    this.formElement = document.querySelector(formSelector);
    this.errors = [];
  }
  setFormFields(formFields) {
    this.#_formFieldInstances = formFields;
  }

  getFormFields() {
    return this.#_formFieldInstances;
  }
  setRules(rules) {
    this.#__rules = rules;
  }
  getRules() {
    return this.#__rules;
  }

  setErrors(errors) {
    this.#_errors = errors;
  }
  getErrors() {
    return this.#_errors;
  }

  setFormData(datalist = []) {
    this.getFormFields.forEach((formFieldInst, idx) => {
      //   set data
      if (this.formElement.querySelector(formFieldInst).tagName === "INPUT") {
        this.formElement.querySelector(formFieldInst).value = datalist[idx];
        return "Set empty into a form with input correctly";
      } else {
        this.formElement.querySelector(formFieldInst).innerText = datalist[idx];
        return "Set empty into a form correctly";
      }
    });
  }

  getFormData() {
    let targetField = [];
    let targetFieldVal = "";
    let returnedData = {};
    this.getFormFields().forEach((formFieldInst) => {
      targetField = formFieldInst["field"];
      if (this.formElement.querySelector(targetField).tagName === "INPUT") {
        targetFieldVal = this.formElement
          .querySelector(targetField)
          .value.trim();
      } else {
        targetFieldVal = this.formElement
          .querySelector(targetField)
          .innerText.trim();
      }
      returnedData[this.formElement.querySelector(targetField).dataset.name] =
        targetFieldVal;
    });
    return returnedData;
  }
  processParamInText(txt, data) {
    const regex = /{([^}]*)}/;
    let param = txt.match(regex);
    return txt.replace(param[0], data);
  }
  processRules(rule_arr, validatorObj) {
    const regex = /(.*?)\[(.*)\]/;
    let flag = true;
    rule_arr.forEach((rule_obj) => {
      Object.entries(rule_obj).forEach(([k, v]) => {
        let fieldName = k;
        let { type, rule, errorEl, errors } = v;
        fieldName = type === 1 ? "#" + fieldName : "." + fieldName;
        validatorObj.setValue(
          this.formElement.querySelector(fieldName).value.trim().toLowerCase()
        );
        this.#_formFieldInstances.push(fieldName);
        let field_rule_arr = rule.split("|");
        let convert_field_rule_arr = [];
        // create rule arr with format {function_name, para_name};
        convert_field_rule_arr = field_rule_arr.reduce((arr, current) => {
          let p = current.match(regex);
          let f = "";
          let para = "";
          if (p) {
            f = p[1];
            para = p[2];
          } else {
            f = current;
            para = null;
          }
          arr.push({ f, para });
          if (!errors.hasOwnProperty(f)) {
            errors[f] = this.#_DEFAULT_ERROR_MESSAGE[f];
            errors[f] = this.processParamInText(errors[f], para);
          }
          return arr;
        }, []);
        console.log(errors);
        this.setRules(convert_field_rule_arr);
        this.setErrors(errors);
        flag &= this.executeRule(errorEl, validatorObj);
      });
    });

    return flag;
  }
  executeRule(fieldError, validatorObj) {
    let ruleArr = this.getRules();
    let errors = this.getErrors();
    console.log(errors);
    if (ruleArr.length > 1) {
      let valid = true;
      ruleArr.forEach((r) => {
        let f = r.f;
        let p = r.para;
        if (valid) {
          if (!validatorObj[f](p)) {
            valid = false;
            document.querySelector(fieldError).innerText = errors[f];
          } else {
            valid = true;
            document.querySelector(fieldError).innerText = "";
          }
        }
      });
      return valid;
    } else if (ruleArr.length === 1) {
      let f = ruleArr[0].f;
      let p = ruleArr[0].para;
      return p === null ? f() : f(p);
    } else {
      console.log("Co loi ve arr");
    }
  }
}

export { FormController };
