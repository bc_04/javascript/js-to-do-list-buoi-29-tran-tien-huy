import {
  renderTodoList,
  sortAscedingOrder,
  sortDescedingOrder,
} from "./controller/Todo.controler.js";
import { Todo } from "./model/Todo.model.js";
import { Storage } from "./utils/Storage.utils.js";
import { FormController } from "./controller/Form.controller.js";
import { Validator } from "./model/Validator.model.js";

let todoList = [];
let storageObj = new Storage();

let initFormController = () => {
  let formControllerObj = new FormController(".card__add");
  let validatorObj = new Validator();
  let rule_arr = [
    {
      newTask: {
        type: 1,
        rule: "required|maxLength[50]",
        errorEl: ".card__error .text-error-input",
        errors: {
          required: "Please enter a task",
        },
      },
    },
  ];

  return formControllerObj.processRules(rule_arr, validatorObj);
};

let addTodo = () => {
  let inputValue = document
    .querySelector("#newTask")
    .value.trim()
    .toLowerCase();
  return new Todo(inputValue, 0);
};

let completeTodo = (e) => {
  let todoIdx = parseInt(e.target.dataset.idx);
  todoList[todoIdx].status = 1;
  storageObj.deleteData("todoList");
  storageObj.saveData("todoList", todoList);
  renderTodoList(todoList, {
    completeTodo,
    deleteTodo,
  });
};

let deleteTodo = (e) => {
  let todoIdx = parseInt(e.target.dataset.idx);
  todoList.splice(todoIdx, 1);
  storageObj.deleteData("todoList");
  storageObj.saveData("todoList", todoList);
  renderTodoList(todoList, { completeTodo, deleteTodo });
};

let loadTodoList = () => {
  todoList = storageObj.loadData("todoList");
  todoList = todoList.map((todo) => {
    return new Todo(todo.name, todo.status);
  });

  renderTodoList(todoList, {
    completeTodo,
    deleteTodo,
  });
};

loadTodoList();

document.querySelector("#addItem").addEventListener("click", () => {
  if (initFormController()) {
    let newTodoObj = addTodo();
    todoList.unshift(newTodoObj);
    console.log(todoList);
    storageObj.saveData("todoList", todoList);
    renderTodoList(todoList, { completeTodo, deleteTodo });
  }
});
document.querySelector(".filter-btn #two").addEventListener("click", () => {
  sortAscedingOrder(todoList);
  renderTodoList(todoList, { completeTodo, deleteTodo });
});
document.querySelector(".filter-btn #three").addEventListener("click", () => {
  sortDescedingOrder(todoList);
  renderTodoList(todoList, { completeTodo, deleteTodo });
});
